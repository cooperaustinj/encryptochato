package encryptoChato;

import encryptoChato.chato.client.Client;
import encryptoChato.chato.server.Server;

public class EncryptoChato {

	private char runMode = 'x'; // x = none | c = client | s = server
	private boolean debugMode = false;

	public static void main(String[] args) {
		EncryptoChato ec = new EncryptoChato(args);

		/*-EncryptoChato ec = new EncryptoChato(new String[] {
				"-m", "client"
		});*/
	}

	public EncryptoChato(String[] args) {
		for (int i = 0; i < args.length; i++) {
			switch (args[i]) {
			case "-m":
				if (args.length <= i) {
					displayErrorExit("You must specify either \"client\" or \"server\" for the -m flag!");
				}
				if (args[i + 1].equalsIgnoreCase("client")) {
					runMode = 'c';
				} else if (args[i + 1].equalsIgnoreCase("server")) {
					runMode = 's';
				}
				i++;
				break;
			case "-d":
				debugMode = true;
				break;
			default:
				displayErrorExit("Invalid flag!");
				break;
			}
		}

		if (debugMode) {
			System.out.println("Running in debug mode.");
		}

		switch (runMode) {
		case 'x':
			displayErrorExit("You must use the -m flag to specify \"client\" or \"server\" mode!");
			break;
		case 'c':
			System.out.println("Running in client mode.");
			new Client();
			break;
		case 's':
			System.out.println("Running in server mode.");
			new Server();
			new Client();
			break;
		default:
			displayErrorExit("Invalid run mode.  This error should be impossible.");
			break;
		}
	}

	public void displayErrorExit(String msg) {
		System.err.println(msg);
		System.exit(-1);
	}
}
