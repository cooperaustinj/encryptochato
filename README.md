Austin Cooper  |  Justin Krause

# Project #1 Proposal

**A.**

  1. a brief description of your project plan, including your gray areas and/or muddy concepts from the prerequisite topics

You should ask yourself the following questions and the answers should help you write your proposal:

**B.**

  1. What exactly do you plan to do in order to address your gray areas and/or muddy concepts meaningfully?
  2. What do you expect to gain/achieve out of it? (your goals and expected outcomes of this project)
  3. What is the motivation behind your proposed idea/plan?


#--------------------------------------------------------------------------#


**A.**
  
  1. We will create a chat program that encrypts and decrypts the data so it is secure while being transferred.  We also plan to have some kind of login system and securely store user data.
  
###Our grey areas:
  - planning a larger project
  - completing a larger project
  - networking/encryption

      
**B.**

  1. Creating a program entirely about networking and encryption
  2. Expect to gain/achieve:
    - experience the design process of a larger project
    - learn the Java Crypto class
    - learn about standardized encryption
    - learn about networking/sockets
    - to create a program with a real world use case
    - to successfully collaborate with a teammate
    - learn about Git and source control
    - learn about safely storing sensitive data (usernames, passwords)
    - to gain experience with Oracle's Java documentation
  3. Encryption is important to the internet, so it will be useful to know and understand how to use it in a program.  In addition, storing sensitive data is pretty much a requirement for any program with a user base, so it is also important to understand.