package encryptoChato.chato.server;

import java.io.IOException;
import java.net.ServerSocket;

public class ClientListener implements Runnable {

	private Server server;
	private ServerSocket serverSocket;

	private boolean listening;

	public ClientListener(Server server, ServerSocket serverSocket) {
		this.server = server;
		this.serverSocket = serverSocket;
		this.listening = true;
		new Thread(this).start();
	}

	@Override
	public void run() {
		while (listening) {
			try {
				server.addClientHandler(new ClientHandler(serverSocket.accept(), server));
			} catch (IOException e) {
				// e.printStackTrace();
				System.out.println("Lost connection!");
				System.exit(-1);
			}
		}
	}
}
