package encryptoChato.chato.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;

import encryptoChato.encrypto.Encrypto;

public class ServerListener implements Runnable {

	private PrintWriter out;
	private BufferedReader in;
	
	public final BigInteger pubExp;
	public final BigInteger pubKey;
	public final BigInteger privKey;
	public final BigInteger prime1, prime2;
	
	private String name;
	private boolean listening;

	public ServerListener(PrintWriter out, BufferedReader in, String name) {
		this.out = out;
		this.in = in;
		this.name = name;
		listening = true;
		prime1 = Encrypto.genPrime();
		prime2 = Encrypto.genPrime();
		pubExp = Encrypto.genPubExp(prime1, prime2);
		pubKey = Encrypto.genPubKey(prime1, prime2);
		privKey = Encrypto.genPrvKey(prime1, prime2, pubExp);
		out.println(pubExp);
		out.println(pubKey);
		new Thread(this).start();
	}

	@Override
	public void run() {
		while (listening) {
			try {
				// System.out.println("\b\b\b\b\b" + Encrypto.bigToString(Encrypto.decrypt(in.readLine(), privKey, pubKey)));
				System.out.println("\b\b\b\b\b\b" + in.readLine());
				System.out.print(name + ": ");
			} catch (IOException e) {
				// e.printStackTrace();
				System.out.println("Disconnected from server!");
				System.exit(0);
			}
		}
	}
}
