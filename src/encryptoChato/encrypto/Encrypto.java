package encryptoChato.encrypto;

import java.math.BigInteger;
import java.security.SecureRandom;

public class Encrypto {

	public static final int bitLength = 32;
	
	// Generates a large Prime number using secureRandom
	public static BigInteger genPrime() {
		return (BigInteger.probablePrime(bitLength, new SecureRandom()));
	}

	// Generates a public key
	public static BigInteger genPubKey(BigInteger num1, BigInteger num2) {
		return num1.multiply(num2);
	}

	// Generates the phi or totient
	public static BigInteger genPhiFactor(BigInteger num1, BigInteger num2) {
		BigInteger phiOne = (num1.subtract(BigInteger.ONE));
		BigInteger phiTwo = (num2.subtract(BigInteger.ONE));
		return phiOne.multiply(phiTwo);

	}

	//Generate the public Exponent
	public static BigInteger genPubExp(BigInteger num1, BigInteger num2){
		BigInteger asdfa = genPhiFactor(num1,num2) ;
		BigInteger lpe = (BigInteger.probablePrime(asdfa.bitLength()-1, new SecureRandom()));
		while(!(asdfa.gcd(lpe).equals(BigInteger.ONE))){
			lpe = (BigInteger.probablePrime(asdfa.bitLength()-1, new SecureRandom()));
		}
		return lpe;
	
	}

	// Generates Private Key
	public static BigInteger genPrvKey(BigInteger num1, BigInteger num2, BigInteger pubExp){
		return pubExp.modInverse(genPhiFactor(num1, num2));
	}

	// Converts string message into BigInteger
	public static BigInteger mesConvToBig(String message) {
		byte[] foo = message.getBytes();
		return new BigInteger(foo);
	}

	// Converts bigInteger to byte[]
	public static byte[] mesConvToByte(BigInteger bigIntMessage) {
		return bigIntMessage.toByteArray();
	}

	// Converts byte array to String
	public static String mesConvToString(byte[] byteMessage) {
		return (new String(byteMessage));
	}
	
	public static String bigToString(BigInteger msg) {
		return mesConvToString(mesConvToByte(msg));
	}

	// Encrypts Message
	public static BigInteger encrypt(BigInteger message, BigInteger pubExp, BigInteger pubKey) {
		return message.modPow(pubExp, pubKey);

	}

	// Decrypts message
	public static BigInteger decrypt(BigInteger encrypted, BigInteger privKey, BigInteger pubKey) {
		return encrypted.modPow(privKey, pubKey);

	}

	// Encrypts Message
	public static BigInteger encrypt(String message, BigInteger pubExp, BigInteger pubKey) {
		return mesConvToBig(message).modPow(pubExp, pubKey);

	}

	// Decrypts message
	public static BigInteger decrypt(String encrypted, BigInteger privKey, BigInteger pubKey) {
		return mesConvToBig(encrypted).modPow(privKey, pubKey);

	}
}
