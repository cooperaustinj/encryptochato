package encryptoChato.chato.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.Socket;
import java.util.Random;

import encryptoChato.encrypto.Encrypto;

public class ClientHandler implements Runnable {

	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;
	private Server server;
	
	public BigInteger clientPubExp;
	public BigInteger clientPubKey;

	private final String name;
	private boolean listening;

	public ClientHandler(Socket socket, Server server) {
		this.socket = socket;
		this.server = server;
		this.name = Character.toString((char) (new Random().nextInt(26) + 65)) + Character.toString((char) (new Random().nextInt(26) + 65)) + Character.toString((char) (new Random().nextInt(26) + 65)) + Character.toString((char) (new Random().nextInt(26) + 65));
		listening = true;
		new Thread(this).start();
	}

	@Override
	public void run() {
		try {
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream(), true);
			out.println(name);
			out.println("Welcome to the server!  Your name is: ");
			out.println(server.pubExp);
			out.println(server.pubKey);
			
			clientPubExp = new BigInteger(in.readLine());
			clientPubKey = new BigInteger(in.readLine());
			
			while (listening) {
				String line = in.readLine();
				for (int i = 0; i < server.clientHandlersSize(); i++) {
					if (server.getClientHandler(i) != this) {
						// server.getClientHandler(i).sendMessage(name + ": " + Encrypto.bigToString(Encrypto.decrypt(line, server.privKey, server.pubKey)));
						server.getClientHandler(i).sendMessage(name + ": " + line);
					}
				}
			}
		} catch (IOException e) {
			// e.printStackTrace();
			listening = false;
		}
	}

	public void sendMessage(String msg) {
		// out.println(Encrypto.encrypt(msg, clientPubExp, clientPubKey));
		out.println(msg);
	}
}
