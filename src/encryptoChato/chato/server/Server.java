package encryptoChato.chato.server;

import java.io.IOException;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.util.ArrayList;

import encryptoChato.encrypto.Encrypto;

public class Server implements Runnable {

	public final int PORT_NUMBER = 12321;

	private ArrayList<ClientHandler> clientHandlers = new ArrayList<ClientHandler>();
	public final BigInteger pubKey;
	public final BigInteger pubExp;
	public final BigInteger privKey;
	public final BigInteger prime1, prime2;

	private boolean listening;

	public Server() {
		prime1 = Encrypto.genPrime();
		prime2 = Encrypto.genPrime();
		pubExp = Encrypto.genPubExp(prime1, prime2);
		pubKey = Encrypto.genPubKey(prime1, prime2);
		privKey = Encrypto.genPrvKey(prime1, prime2, pubExp);
		new Thread(this).start();
	}

	@Override
	public void run() {
		try {
			ServerSocket serverSocket = new ServerSocket(PORT_NUMBER);
			new ClientListener(this, serverSocket);
			while (listening) {
				for (int i = 0; i < clientHandlersSize(); i++) {
					// if (clientHandlers.get(i).isConnected())
					// clientHandlers.get(i).sendMessage("Hello there");
				}
				Thread.sleep(1000);
			}
		} catch (IOException e) {
			// e.printStackTrace();
			System.out.println("Unable to connect!");
			System.exit(-1);
		} catch (InterruptedException e) {
			// e.printStackTrace();
			System.out.println("Unable to connect!");
			System.exit(-1);
		}
	}

	public void addClientHandler(ClientHandler ch) {
		clientHandlers.add(ch);
	}

	public ClientHandler getClientHandler(int i) {
		return clientHandlers.get(i);
	}

	public int clientHandlersSize() {
		return clientHandlers.size();
	}
}
