package encryptoChato.chato.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.Socket;

import encryptoChato.encrypto.Encrypto;

public class Client implements Runnable {

	public final int PORT_NUMBER = 12321;
	
	public String name;

	public BigInteger servPubKey;
	public BigInteger servPubExp;

	private boolean listening;

	public Client() {
		listening = true;
		new Thread(this).start();
	}

	@Override
	public void run() {
		try {
			Socket socket = new Socket("localhost", PORT_NUMBER);
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));

			System.out.println("Connected to server!");
			
			name = in.readLine();
			System.out.println(in.readLine() + name + "\n\n");
			servPubExp = new BigInteger(in.readLine());
			servPubKey = new BigInteger(in.readLine());
			new ServerListener(out, in, name);
			
			String msg = "";
			while (listening) {
				System.out.print(name + ": ");
				msg = stdIn.readLine();
				if(msg.isEmpty()) {
					continue;
				}
				// out.println(Encrypto.encrypt(msg, servPubExp, servPubKey));
				out.println(msg);
			}

			/*-String userInput;
			while ((userInput = stdIn.readLine()) != null) {
				out.println(userInput);
			}*/
		} catch (IOException e) {
			// e.printStackTrace();
			System.exit(0);
		}
	}
}
