# References

* * *

- [Java Docs](https://docs.oracle.com/javase/7/docs/api/)

- [Lesson: All About Sockets](https://docs.oracle.com/javase/tutorial/networking/sockets/)

- [Lesson: Concurrency](https://docs.oracle.com/javase/tutorial/essential/concurrency/)